from app import db
from app import ma
import datetime


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50))
    surname = db.Column(db.String(50))
    company = db.Column(db.String(50))
    address = db.Column(db.String(200))
    code = db.Column(db.String(10))
    city = db.Column(db.String(50))
    phone = db.Column(db.String(10))
    nip = db.Column(db.String(10))
    email = db.Column(db.String(50))
    login = db.Column(db.String(50))
    password = db.Column(db.String(50))


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User


class Way(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    name = db.Column(db.String(50))
    start = db.Column(db.String(200))
    end = db.Column(db.String(200))
    full_distance = db.Column(db.FLOAT)
    created = db.Column(db.DateTime, default=datetime.datetime.now())


class WaySchema(ma.ModelSchema):
    class Meta:
        model = Way


class WayPoint(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    way_id = db.Column(db.Integer, db.ForeignKey('way.id'))
    address = db.Column(db.String(200))


class WayPointSchema(ma.ModelSchema):
    class Meta:
        model = WayPoint
