'use strict';
var app = angular.module('application',
	[
		'ngRoute',
		'angular-storage',
		'authorization-cont',
		'make-way-cont',
		'show-ways-cont'
	]);


app.config(['$routeProvider','$locationProvider','$qProvider',function($routeProvider, $locationProvider,$qProvider) {
	$routeProvider.when('/', {
		templateUrl : '../static/partials/auth/sign_in.html',
		controller : 'sign-in',
	});
	$routeProvider.when('/register', {
		templateUrl : '../static/partials/auth/register.html',
		controller : 'register',
	});
	$routeProvider.when('/site/edit-user', {
		templateUrl : '../static/partials/auth/edit_user.html',
		controller : 'edit-user',
	});
	$routeProvider.when('/site/make-way', {
		templateUrl : '../static/partials/site/make_way.html',
		controller : 'make-way',
	});

	$routeProvider.when('/site/show-ways', {
		templateUrl : '../static/partials/site/show_ways.html',
		controller : 'show-ways',
	});
	$routeProvider.when('/site/show-way/:id', {
		templateUrl : '../static/partials/site/show_way.html',
		controller : 'show-way',
	});
		$routeProvider.when('/contact', {
		templateUrl : '../static/partials/site/contact.html',
		controller : 'contact',
	});

	$locationProvider.html5Mode(false);
	$qProvider.errorOnUnhandledRejections(false);
}]);


