var auth = angular.module('authorization-cont', ['ngRoute']);

auth.controller('sign-in',['$scope','$http','$timeout','$location','store', function($scope,$http,$timeout,$location,store){
	if (store.get('user')){
		$location.path("/site/make-way");
	}
    $scope.signIn = function () {
        let input = $scope.input;
        $http.post('/api/auth',input).then(function(response){
            $scope.message = response.data.message;
            store.set('user',response.data.user);
            $timeout(function() { $location.path("/site/make-way");}, 3000);
		}),function(){
            $scope.message = 'Nieoczekiwany błąd'
		};
    }
}]);
auth.controller('edit-user',['$scope','$http','$timeout','$location','store', function($scope,$http,$timeout,$location,store){
	$scope.input = store.get('user');
	$scope.updateUser = function () {
        let input = $scope.input;

        if (input.name === '' || input.name === undefined){
        	$scope.message = 'Wypełnij imię'
		}
        else if (input.surname === '' || input.surname === undefined){
        	$scope.message = 'Wypełnij nazwisko'
		}
        else if (input.company === '' || input.company === undefined){
        	$scope.message = 'Wypełnij nazwę firmy'
		}
        else if (input.address === '' || input.address === undefined){
        	$scope.message = 'Wypełnij adres'
		}
        else if (input.code === '' || input.code === undefined){
        	$scope.message = 'Wypełnij kod pocztowy'
		}
        else if (input.city === '' || input.city === undefined){
        	$scope.message = 'Wypełnij miasto'
		}
        else if (input.phone === '' || input.phone === undefined){
        	$scope.message = 'Wypełnij telefon'
		}
        else if (input.nip === '' || input.nip === undefined){
        	$scope.message = 'Wypełnij NIP'
		}
        else if (input.email === '' || input.email === undefined){
        	$scope.message = 'Wypełnij email'
		}
        else if (input.password === '' || input.password === undefined){
        	$scope.message = 'Wypełnij hasło'
		}
        else
		{
			delete $scope.message;
			input.user_id = store.get('user').id;
			$http.post('/api/edit-user',input).then(function(response){
				$scope.message = response.data.message;
				store.set('user',response.data.user);
				$timeout(function() { $location.path("/site/show-ways");}, 4000);
			}),function(){
				$scope.message = 'Nieoczekiwany błąd'
			};
		}

    }
}]);
auth.controller('register', ['$scope','$http','$timeout','$location', function($scope,$http,$timeout,$location){
    $scope.register = function () {
        let input = $scope.input;
        input.password = $scope.input.password1;

        if (input.name === '' || input.name === undefined){
        	$scope.message = 'Wypełnij imię'
		}
        else if (input.surname === '' || input.surname === undefined){
        	$scope.message = 'Wypełnij nazwisko'
		}
        else if (input.company === '' || input.company === undefined){
        	$scope.message = 'Wypełnij nazwę firmy'
		}
        else if (input.address === '' || input.address === undefined){
        	$scope.message = 'Wypełnij adres'
		}
        else if (input.code === '' || input.code === undefined){
        	$scope.message = 'Wypełnij kod pocztowy'
		}
        else if (input.city === '' || input.city === undefined){
        	$scope.message = 'Wypełnij miasto'
		}
        else if (input.phone === '' || input.phone === undefined){
        	$scope.message = 'Wypełnij telefon'
		}
        else if (input.nip === '' || input.nip === undefined){
        	$scope.message = 'Wypełnij NIP'
		}
        else if (input.email === '' || input.email === undefined){
        	$scope.message = 'Wypełnij email'
		}
        else if (input.login === '' || input.login === undefined){
        	$scope.message = 'Wypełnij login'
		}
        else if (input.password === '' || input.password === undefined){
        	$scope.message = 'Wypełnij hasło'
		}

        else if (input.password1 !== input.password2){
            $scope.message = 'Podane hasła się nie zgadzają'
        }
        else{
            $http.post('/api/crete-user',input).then(function(response){
                $scope.message = response.data;
                $timeout(function() { $location.path("/");}, 3000);
            }),function(){
                $scope.message = 'Nieoczekiwany błąd'
            };
        }
    }
}]);

auth.controller('navigation', ['$scope','$location','store', function($scope,$location,store){
	if (store.get('user')){
		$scope.username = store.get('user').login;
	}
	$scope.navigations = function(){
		var patt = new RegExp("^\/site");
		if(patt.test($location.path()))
			if(store.get('user')){
				return "../static/partials/share/navigation.html";
			}else
			{
				$location.path('/');
			}
	};

	$scope.logout = function(){
		store.remove('user');
		$location.path("/");
	};

	$scope.isActive = function ( path ) {
		var result = null;
		if(Array.isArray(path))
		{
			path.forEach(function(item)
			{
				if($location.path() === item)
					result = "active" ;
			});
		}
		else
		{
			if($location.path() === path)
				result =  "active" ;
		}
		return result;
	};

}]);
