var makeWay = angular.module('make-way-cont', ['ngRoute']);

makeWay.controller('make-way', ['$scope','$http','$timeout','$location','store', function($scope,$http,$timeout,$location,store){
    $scope.waypoints = [];
    $scope.map = {};

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    let map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 51.759445,
            lng: 19.457216
        },
        zoom: 7
    });
    directionsDisplay.setMap(map);

    let start_input = document.getElementById('add-start');

    let startAutocomplete = new google.maps.places.Autocomplete(start_input);

    let end_input = document.getElementById('add-end');
    let endAutocomplete = new google.maps.places.Autocomplete(end_input);

    let waypoint_input = document.getElementById('add-waypoint');
    let waypointAutocomplete = new google.maps.places.Autocomplete(waypoint_input);

    startAutocomplete.bindTo('bounds', map);
    endAutocomplete.bindTo('bounds', map);
    waypointAutocomplete.bindTo('bounds', map);

    startAutocomplete.addListener('place_changed', function() {
        $scope.map.start.obj = startAutocomplete.getPlace();
        checkPoints();
    });
    endAutocomplete.addListener('place_changed', function() {
        $scope.map.end.obj = endAutocomplete.getPlace();
        checkPoints();
    });


    checkPoints = function(){
        if ($scope.map.start !== undefined && $scope.map.end !== undefined){
            let start = $scope.map.start.obj;
            let end = $scope.map.end.obj;

            if (start && end ){
               if (start.geometry && end.geometry){
                   let waypointsLocation = $scope.waypoints.map(function(waypoint){
                        let output = {};
                        output.location = waypoint.geometry.location;
                        output.stopover = true;
                        return output;
                   });

                   showWay(start.geometry.location,end.geometry.location,waypointsLocation)
               }
            }
        }
    };

    showWay = function (start,end, waypoints) {
        directionsService.route({
            origin: start,
            destination: end,
            waypoints: waypoints,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                if (response['routes'][0]['legs']){
                    $scope.map.fullDistance = getFullDistance(response['routes'][0]['legs']);
                    $scope.map.parts = response['routes'][0]['legs'];
                }
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

    };

    getFullDistance = function(legs){
        let distance = legs.reduce((a, b) => a + (b['distance']['value'] || 0), 0);
        return distance/1000;
    };
    $scope.addWaypoint = function () {
        if (waypoint_input.value !== ''){
             $scope.waypoints.push(waypointAutocomplete.getPlace());
            delete $scope.map.waypoint.input;
            checkPoints();
        }
    };
    $scope.removeWaypoint = function (index) {
        $scope.waypoints.splice(index,1);
        checkPoints();
    };

    $scope.saveWay = function () {
        if($scope.map.name && $scope.map.start && $scope.map.end){
            let data = {
                'map':{
                    'name': $scope.map.name,
                    'start': $scope.map.start.obj,
                    'end': $scope.map.end.obj,
                    'waypoints': $scope.waypoints,
                    'full_distance':$scope.map.fullDistance
                },
                'user_id':store.get('user').id
            };
            $http.post('/api/create-way',data).then(function(response){
                $scope.message = response.data.message;
                $timeout(function() { delete $scope.message;}, 3000);
            }),function(){
                $scope.message = 'Nieoczekiwany błąd'
            };
        }else
        {
            $scope.message = 'Uzupełnij wszystkie pola!';
            $timeout(function() { delete $scope.message;}, 5000);
        }
    }
}]);

