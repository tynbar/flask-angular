var showWay = angular.module('show-ways-cont', ['ngRoute']);

showWay.controller('show-ways',['$scope','$http','$timeout','$location','store', function($scope,$http,$timeout,$location,store){
    let data = {'user_id': store.get('user').id};

	$http.post('/api/get-ways',data).then(function(response){
        $scope.waysList = response.data;

    }),function(){
        $scope.message = 'Nieoczekiwany błąd'
    };
	$scope.getDate = function(date){
		var object = new Date(date);
		return addZero(object.getDate())+"-"+addZero(object.getMonth()+1)+"-"+object.getFullYear();
	}
}]);

showWay.controller('show-way',['$scope','$http','$timeout','$location','store','$routeParams', function($scope,$http,$timeout,$location,store,$routeParams){
    $scope.state = true;
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    let map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 51.759445,
            lng: 19.457216
        },
        zoom: 7
    });

    directionsDisplay.setMap(map);

    $http.get('/api/get-way/'+$routeParams.id).then(function(response){
        $scope.way = response.data;
        let waypoints = $scope.way.waypoints.map(function(waypoint){
            let output = {};
            output.location = waypoint.address;
            output.stopover = true;
            return output;
        });
        showWay($scope.way.start,$scope.way.end,waypoints)
    }),function(){
        $scope.message = 'Nieoczekiwany błąd'
    };

    showWay = function (start,end, waypoints) {
        directionsService.route({
            origin: start,
            destination: end,
            waypoints: waypoints,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                $scope.way.parts = response['routes'][0]['legs'];
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    };

    $scope.showParts = function() {
        $scope.state = false;
    };

    $scope.getDate = function(date){
		var object = new Date(date);
		return addZero(object.getDate())+"-"+addZero(object.getMonth()+1)+"-"+object.getFullYear();
	};


}]);

function addZero(number){
	if(number<10)
		return "0"+number;
	else
		return number;
}
