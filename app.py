from flask import Flask, send_file, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os
import json

app = Flask(__name__)
app.config['SECRET_KEY'] = '774d1eb55a45e8c2e9'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(os.getcwd(), 'sqlite.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(db)

from models import User, UserSchema
from models import Way, WaySchema
from models import WayPoint, WayPointSchema


@app.route('/')
def index():
    return send_file('templates/index.html')


@app.route('/api/crete-user', methods=['POST'])
def crete_user():
    request_data = request.json
    existing = User.query.filter(User.login == request.json.get('login')).first()
    if not existing:
        new_user = User(
            name=request_data.get('name', ""),
            surname=request_data.get('surname', ""),
            company=request_data.get('company', ""),
            address=request_data.get('address', ""),
            code=request_data.get('code', ""),
            city=request_data.get('city', ""),
            phone=request_data.get('phone', ""),
            nip=request_data.get('nip', ""),
            email=request_data.get('email', ""),
            login=request_data.get('login', ""),
            password=request_data.get('password', ""))

        db.session.add(new_user)
        db.session.commit()

        if new_user:
            message = 'Utworzono pomyślnie'
        else:
            message = 'Nieoczekiwany błąd'
    else:
        message = 'Użytkownik o loginie: {0} już istnieje'.format(request_data.get('login'))

    return message


@app.route('/api/edit-user', methods=['POST'])
def edit_user():
    request_data = request.json
    user_schema = UserSchema()
    user = User.query.filter(User.id == request.json.get('user_id')).first()

    if user:
        user.name = request_data.get('name', "")

        user.surname = request_data.get('surname', "")
        user.company = request_data.get('company', "")
        user.address = request_data.get('address', "")
        user.code = request_data.get('code', "")
        user.city = request_data.get('city', "")
        user.phone = request_data.get('phone', "")
        user.nip = request_data.get('nip', "")
        user.email = request_data.get('email', "")
        user.password = request_data.get('password', "")
        db.session.commit()

        user_obj = user_schema.dump(user).data
        del user_obj['password']
        output = {'user': user_obj, 'message': 'Zaktualizowano pomyślnie!'}

    else:
        pass
        output = {'message': 'Użytkownik o loginie: {0} już istnieje'.format(request_data.get('login'))}

    return output


@app.route('/api/auth', methods=['POST'])
def authorization():
    request_data = request.json
    user_schema = UserSchema()
    user = User.query.filter(User.login == request_data.get('login')).first()

    if user and user.password == request_data.get('password'):
        user_obj = user_schema.dump(user).data
        del user_obj['password']
        output = {'user': user_obj, 'message': 'Zalogowano pomyślnie!'}
    else:
        output = {'message': 'Błędny login lub hasło!'.format(request_data.get('login'))}

    return output


@app.route('/api/create-way', methods=['POST'])
def create_way():
    request_data = request.json
    map = request_data.get('map')
    way = Way(
        user_id=request_data.get('user_id'),
        name=map.get('name', ""),
        start=map.get('start').get('formatted_address', ""),
        end=map.get('end').get('formatted_address', ""),
        full_distance=map.get('full_distance', 0)
    )
    db.session.add(way)
    db.session.flush()

    for waypoint_obj in map.get('waypoints'):
        waypoint = WayPoint(way_id=way.id, address=waypoint_obj.get('formatted_address', ""))
        db.session.add(waypoint)

    db.session.commit()
    return {'message': 'Dodano pomyślnie!'}


@app.route('/api/get-ways', methods=['POST'])
def get_ways():
    request_data = request.json
    way_schema = WaySchema(many=True)
    waypoint_schema = WayPointSchema(many=True)

    ways = Way.query.filter(Way.user_id == request_data.get('user_id')).all()
    ways_obj = way_schema.dump(ways).data
    for way in ways_obj:
        waypoints = WayPoint.query.filter(WayPoint.way_id == way.get('id')).all()
        way['waypoints'] = waypoint_schema.dump(waypoints).data
    return jsonify(ways_obj)


@app.route('/api/get-way/<id>', methods=['GET'])
def get_way(id):
    way_schema = WaySchema()
    waypoint_schema = WayPointSchema(many=True)

    way = Way.query.filter(Way.id == id).first()
    way_obj = way_schema.dump(way).data
    waypoints = WayPoint.query.filter(WayPoint.way_id == way.id).all()
    way_obj['waypoints'] = waypoint_schema.dump(waypoints).data
    return jsonify(way_obj)


@app.route('/api/get-users', methods=['GET'])
def get_users():
    user_schema = UserSchema(many=True)
    return jsonify(user_schema.dump(User.query.all()).data)


if __name__ == '__main__':
    app.run(debug=True)
